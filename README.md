# README #

Desafio Java [Hands On] 

### Projeto ###

 API RESTful para gerir dados de armazenamento e estoque de um depósito de bebidas.

### Objetivos ###

* Cadastro e consulta das bebidas armazenadas em cada seção com suas respectivas queries. 

* Consulta do volume total no estoque por cada tipo de bebida. 

* Consulta dos locais disponíveis de armazenamento de um determinado volume de bebida. 

* Consulta das seções disponíveis para venda de determinado tipo de bebida. 

* Consulta do histórico de entradas e saídas por tipo de bebida e seção. 


### Rodar Aplicação

* Navegar até o diretório do projeto
* Executar o seguinte comando

$ mvn spring-boot:run

##Objetos

* Beverage ( usado para criar bebida)


	{
	  "userName" : "kamila_j",
	  "beverageDescription" : "vinho concha",
	  "code" : "000005",
	  "name" : "vinho concha",  
	  "alcoholic" : 1,
	  "bottleSize" : 100000
	}

* SellBeverage ( objeto de registro de venda)

	{
	  "idBeverage" : "kamila_j",
	  "userName" : "vinho concha"
	}

* HistoryStockBeverage ( objeto de retorno de histórico)

	{
	  "idBeverage" : "kamila_j",
	  "userName" : "vinho concha"
	}

## Endpoints


| HTTP Verb     | URL         | Description  | Status Codes |
| ------------- |-------------|:-----        | ----         |
| `GET` | `http://localhost:8080/beverage/findAll` | Retorna todas as bebidas no estoque |`200 OK` |
| `POST` | `http://localhost:8080/beverage/create/{BeverageDto}` | Cadastra bebida no estoque | `200 OK` |
| `DELETE` | `http://localhost:8080/beverage/selling/{SellBeverageDto}` | Retira bebida no estoque | `200 OK` |
| `GET` | `http://localhost:8080/beverage/findVolumeByType/{isAlcoholic}` | Consulta do volume total no estoque por tipo de bebida | `200 OK` |
| `POST` | `http://localhost:8080/beverage/create/{BeverageDto}` | Cadastra sessão | `200 OK` |
| `GET` | `http://localhost:8080/beveragesession/findSessionsAvailable/{isAlcoholic}{isAlcoholic}` | Consulta dos locais disponíveis de armazenamento de um determinado volume de bebida | `200 OK` |
