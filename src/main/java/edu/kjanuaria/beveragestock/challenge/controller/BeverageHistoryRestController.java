package edu.kjanuaria.beveragestock.challenge.controller;

import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.kjanuaria.beveragestock.challenge.entity.HistoryStockBeverage;
import edu.kjanuaria.beveragestock.challenge.entity.dto.HistoryStockBeverageDto;
import edu.kjanuaria.beveragestock.challenge.service.IHistoryBeverage;
import edu.kjanuaria.beveragestock.challenge.util.ConverterUtils;

@RestController
@RequestMapping("/beveragehistory")
public class BeverageHistoryRestController {

	@Autowired
	private IHistoryBeverage iHistoryBeverage;

	@ResponseBody
	@RequestMapping(value = "/searchByType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<HistoryStockBeverageDto>> listar(boolean isAlcoholic) {
		List<HistoryStockBeverageDto> historyStockBeverageDtos = ConverterUtils
				.convertEntityToDto((List<HistoryStockBeverage>) iHistoryBeverage.findAllByIsAlcoholic(isAlcoholic));
		return new ResponseEntity<List<HistoryStockBeverageDto>>(historyStockBeverageDtos, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/searchBySession", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<HistoryStockBeverageDto>> listar(String session) {
		List<HistoryStockBeverageDto> historyStockBeverageDtos = ConverterUtils
				.convertEntityToDto((List<HistoryStockBeverage>) iHistoryBeverage.findAllBySession(session));
		return new ResponseEntity<List<HistoryStockBeverageDto>>(historyStockBeverageDtos, HttpStatus.OK);
	}

}
