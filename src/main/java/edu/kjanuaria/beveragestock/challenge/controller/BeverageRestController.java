package edu.kjanuaria.beveragestock.challenge.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import edu.kjanuaria.beveragestock.challenge.entity.Beverage;
import edu.kjanuaria.beveragestock.challenge.entity.dto.BeverageDto;
import edu.kjanuaria.beveragestock.challenge.entity.dto.SellBeverageDto;
import edu.kjanuaria.beveragestock.challenge.service.IBeverage;
import edu.kjanuaria.beveragestock.challenge.util.ConverterUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/beverage")
public class BeverageRestController {

	@Autowired
	private IBeverage beverageDAO;

	@ResponseBody
	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<Beverage> listar(Integer id) {
		return new ResponseEntity<Beverage>(beverageDAO.findById(id).get(), HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/findAll", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Beverage>> findAll() {
		List<Beverage> beverageList = new ArrayList<Beverage>();
		beverageDAO.findAll().iterator().forEachRemaining(beverageList::add);
		return new ResponseEntity<List<Beverage>>(beverageList, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/findAllByType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<Beverage>> findAllByType(String type) {
		List<Beverage> beverageList = new ArrayList<Beverage>();
		beverageDAO.findAllByType(type).iterator().forEachRemaining(beverageList::add);
		return new ResponseEntity<List<Beverage>>(beverageList, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/findVolumeByType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<BigDecimal> findQuantityByType(Boolean isAlcoholic) {
		BigDecimal quantityByType = ConverterUtils.convertToLiters(beverageDAO.countBeverageByType(isAlcoholic));
		return new ResponseEntity<BigDecimal>(quantityByType, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<BeverageDto> create(@Valid @RequestBody BeverageDto beverage) {
		BeverageDto saved = ConverterUtils.convertEntityToDto(beverageDAO.storeBeverage(beverage));
		return new ResponseEntity<BeverageDto>(saved, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/delete", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(Integer id) {
		try {
			Beverage beverage = new Beverage(id);
			beverageDAO.delete(beverage);
		} catch (Exception ex) {
			String error = "Error deleting the Beverage: " + ex.toString();
			return new ResponseEntity<String>(error, HttpStatus.EXPECTATION_FAILED);
		}
		return new ResponseEntity<String>("Beverage " + id + "succesfully deleted!", HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/selling", method = RequestMethod.DELETE)
	public ResponseEntity<String> venda(@RequestBody SellBeverageDto sellBeverageDto) {
		beverageDAO.sellBeverage(sellBeverageDto.getIdBeverage(), sellBeverageDto.getUserName());
		return new ResponseEntity<String>("Beverage " + sellBeverageDto.getIdBeverage() + "succesfully deleted!",
				HttpStatus.OK);
	}

}
