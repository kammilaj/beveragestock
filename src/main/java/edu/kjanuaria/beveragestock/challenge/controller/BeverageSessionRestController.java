package edu.kjanuaria.beveragestock.challenge.controller;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.kjanuaria.beveragestock.challenge.entity.BeverageSession;
import edu.kjanuaria.beveragestock.challenge.entity.dto.BeverageSessionDto;
import edu.kjanuaria.beveragestock.challenge.exception.MaxSessionsException;
import edu.kjanuaria.beveragestock.challenge.service.IBeverageSession;
import edu.kjanuaria.beveragestock.challenge.util.BeverageUtils;
import edu.kjanuaria.beveragestock.challenge.util.ConverterUtils;
import net.logstash.logback.encoder.org.apache.commons.lang.ObjectUtils;

@RestController
@RequestMapping("/beveragesession")
public class BeverageSessionRestController {
	@Autowired
	private IBeverageSession beverageSessionDAO;

	@ResponseBody
	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<BeverageSession> listar(Integer id) {
		return new ResponseEntity<BeverageSession>(beverageSessionDAO.findById(id).get(), HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<BeverageSession>> listar() {
		List<BeverageSession> beverageList = new ArrayList<BeverageSession>();
		beverageSessionDAO.findAll().iterator().forEachRemaining(beverageList::add);
		return new ResponseEntity<List<BeverageSession>>(beverageList, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/findSessionsAvailableByType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<BeverageSession>> findSessionsAvailableByType() {
		List<BeverageSession> beverageList = new ArrayList<BeverageSession>();
		beverageSessionDAO.findAll().iterator().forEachRemaining(beverageList::add);
		return new ResponseEntity<List<BeverageSession>>(beverageList, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/findSessionsAvailable", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<List<BeverageSessionDto>> findSessionsAvailable(Long volume, boolean alcoholic) {
		List<BeverageSessionDto> availableSessions = ConverterUtils.convertListEntityToDto(
				(List<BeverageSession>) beverageSessionDAO.findSessionsAvailable(volume, alcoholic));
		return new ResponseEntity<List<BeverageSessionDto>>(availableSessions, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<BeverageSession> create(String code, boolean alcoholic) {
		BeverageSession beverage = new BeverageSession(code, BeverageUtils.getCapacity(alcoholic), alcoholic);
		List<BeverageSession> allsessions = (List<BeverageSession>) beverageSessionDAO.findAll();
		if (ObjectUtils.equals(5, allsessions.size())) {
			throw new MaxSessionsException("Apenas 5 sessões podem ser criadas");
		}
		BeverageSession saved = beverageSessionDAO.save(beverage);
		return new ResponseEntity<BeverageSession>(saved, HttpStatus.OK);
	}

	@ResponseBody
	@RequestMapping(value = "/deleteAll", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON)
	public ResponseEntity<String> delete() {
		beverageSessionDAO.deleteAll();
		return new ResponseEntity<String>("All deleted", HttpStatus.OK);
	}

	// TODO Consulta das seções disponíveis para venda

}
