package edu.kjanuaria.beveragestock.challenge.controller;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import edu.kjanuaria.beveragestock.challenge.exception.BeverageNotFoundException;
import edu.kjanuaria.beveragestock.challenge.exception.ErrorDetails;
import edu.kjanuaria.beveragestock.challenge.exception.MaxSessionsException;
import edu.kjanuaria.beveragestock.challenge.exception.SessionDontSupportVolumeException;
import edu.kjanuaria.beveragestock.challenge.exception.SessionNotAvailableException;
import edu.kjanuaria.beveragestock.challenge.exception.SessionNotFoundException;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	@ExceptionHandler
	public final ResponseEntity<ErrorDetails> handleSessionNotFoundException(SessionNotFoundException ex,
			WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}

	@ExceptionHandler
	public final ResponseEntity<ErrorDetails> handleSessionDontSupportVolumeException(
			SessionDontSupportVolumeException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	public final ResponseEntity<ErrorDetails> handleSessionNotAvailableException(SessionNotAvailableException ex,
			WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	public final ResponseEntity<ErrorDetails> handleMaxSessionsException(MaxSessionsException ex, WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler
	public final ResponseEntity<ErrorDetails> handleBeverageNotFoundException(BeverageNotFoundException ex,
			WebRequest request) {
		ErrorDetails errorDetails = new ErrorDetails(new Date(), ex.getMessage(), request.getDescription(false));
		return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	}
}
