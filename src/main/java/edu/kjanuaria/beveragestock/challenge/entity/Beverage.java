package edu.kjanuaria.beveragestock.challenge.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "BEVERAGE")
public class Beverage {

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "jpaSequenceOrders", sequenceName = "JPA_SEQUENCE_ORDERS", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequenceOrders")
	public Integer id;

	@Column(name = "BOTTLE_SIZE")
	private Long bottleSize;

	@Column(name = "CODE")
	private String code;

	@Column(name = "NAME")
	private String name;

	@Column(name = "beverage_type")
	private String type;

	@Column(name = "ALCOHOLIC")
	@Type(type = "org.hibernate.type.BooleanType")
	private boolean alcoholic;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "beverageSession")
	private BeverageSession beverageSession;

	public Beverage(Long price, String description, String code, String type, boolean alcoholic) {
		this.code = code;
		this.type = type;
		this.alcoholic = alcoholic;
	}

	public Beverage(String code, boolean alcoholic, Long bottleSize) {
		this.code = code;
		this.alcoholic = alcoholic;
		this.bottleSize = bottleSize;
	}

	public Beverage(Integer id) {
		this.id = id;
	}

	public Beverage(String code, String name, String type, boolean alcoholic, Long bottleSize) {
		this.code = code;
		this.name = name;
		this.type = type;
		this.alcoholic = alcoholic;
		this.bottleSize = bottleSize;
	}

	public Beverage() {
		// TODO Auto-generated constructor stub
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public Long getBottleSize() {
		return bottleSize;
	}

	public void setBottleSize(Long bottleSize) {
		this.bottleSize = bottleSize;
	}

	public BeverageSession getBeverageSession() {
		return beverageSession;
	}

	public void setBeverageSession(BeverageSession beverageSession) {
		this.beverageSession = beverageSession;
	}

}
