package edu.kjanuaria.beveragestock.challenge.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "BEVERAGE_SESSION")
public class BeverageSession {

	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "jpaSequenceOrders", sequenceName = "JPA_SEQUENCE_ORDERS", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequenceOrders")
	private Integer id;

	@NotNull
	@Column(name = "BOL_ALCOHOLIC")
	@Type(type = "org.hibernate.type.BooleanType")
	private boolean alcoholic;

	@Column(name = "QUANTITY")
	private Long quantity = 0L;

	@NotNull
	@Column(name = "CAPACITY")
	private Integer capacity;

	@Column(name = "NAME")
	private String name;

	@OneToMany
	private List<HistoryStockBeverage> historyStockBeverages = new ArrayList<HistoryStockBeverage>();

	public BeverageSession() {
		// TODO Auto-generated constructor stub
	}

	public BeverageSession(Integer id) {
		this.id = id;
	}

	public BeverageSession(String name, Integer capacity, boolean alcoholic) {
		super();
		this.capacity = capacity;
		this.name = name;
		this.alcoholic = alcoholic;
	}

	public BeverageSession(Integer id, boolean alcoholic, Long quantity, Integer capacity, String name) {
		super();
		this.id = id;
		this.alcoholic = alcoholic;
		this.quantity = quantity;
		this.capacity = capacity;
		this.name = name;
	}

	public BeverageSession(boolean alcoholic, Long quantity, Integer capacity, String name) {
		super();
		this.alcoholic = alcoholic;
		this.quantity = quantity;
		this.capacity = capacity;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public List<HistoryStockBeverage> getHistoryStockBeverages() {
		return historyStockBeverages;
	}

	public void setHistoryStockBeverages(List<HistoryStockBeverage> historyStockBeverages) {
		this.historyStockBeverages = historyStockBeverages;
	}

}
