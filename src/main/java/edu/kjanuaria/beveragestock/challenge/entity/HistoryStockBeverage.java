package edu.kjanuaria.beveragestock.challenge.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import edu.kjanuaria.beveragestock.challenge.enumerators.BeverageStateEnum;

@Entity
@Table(name = "HISTORY_STOCK_BEVERAGE")
public class HistoryStockBeverage {
	@Id
	@Column(name = "ID")
	@SequenceGenerator(name = "jpaSequenceOrders", sequenceName = "JPA_SEQUENCE_ORDERS", allocationSize = 1, initialValue = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jpaSequenceOrders")
	public Integer id;

	@Column(name = "user_name")
	private String userName;

	@Column(name = "ALCOHOLIC")
	private Boolean alcoholic;

	@Column(name = "HOUR")
	private Timestamp hour;

	@Column(name = "VOLUME")
	private Long volume;

	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId
	@JoinColumn(name = "BEVERAGE_SESSION_ID")
	private BeverageSession beverageSession;

	@Column(name = "STATE")
	@Enumerated(EnumType.STRING)
	private BeverageStateEnum state;

	public HistoryStockBeverage() {
		// TODO Auto-generated constructor stub
	}

	public HistoryStockBeverage(Long volume, Boolean alcoholic, BeverageSession beverageSession,
			BeverageStateEnum stateEnum, Timestamp hour, String userName) {
		this.volume = volume;
		this.beverageSession = beverageSession;
		this.hour = hour;
		this.userName = userName;
		this.alcoholic = alcoholic;
		this.state = stateEnum;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Boolean getAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(Boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public Timestamp getHour() {
		return hour;
	}

	public void setHour(Timestamp hour) {
		this.hour = hour;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public BeverageSession getBeverageSession() {
		return beverageSession;
	}

	public void setBeverageSession(BeverageSession beverageSession) {
		this.beverageSession = beverageSession;
	}

	public BeverageStateEnum getState() {
		return state;
	}

	public void setState(BeverageStateEnum state) {
		this.state = state;
	}
}
