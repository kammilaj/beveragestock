package edu.kjanuaria.beveragestock.challenge.entity.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import edu.kjanuaria.beveragestock.challenge.entity.Beverage;

public class BeverageDto {

	@NotNull
	@Size(min = 1, message = "user_name can't be null")
	private String userName;
	@NotNull
	@Size(min = 1, message = "beverage description can't be null")
	private String beverageDescription;
	@NotNull(message = "alcoholic can't be null")
	private boolean alcoholic;
	@NotNull
	@NotNull(message = "bottleSize can't be null")
	private Long bottleSize;
	@NotNull
	private BeverageSessionDto beverageSessionDto;

	public BeverageDto() {
		// TODO Auto-generated constructor stub
	}

	public BeverageDto(Beverage beverage) {
		this(beverage.getName(), beverage.isAlcoholic(), beverage.getBottleSize(),
				beverage.getBeverageSession().getQuantity(), beverage.getBeverageSession().getCapacity(),
				beverage.getBeverageSession().getName());
	}

	public BeverageDto(String beverageDescription, boolean alcoholic, Long bottleSize, Long quantity, Integer capacity,
			String name) {
		this.alcoholic = alcoholic;
		this.beverageDescription = beverageDescription;
		this.bottleSize = bottleSize;
		this.beverageSessionDto = new BeverageSessionDto(name, alcoholic, quantity, capacity);
	}

	public BeverageDto(String userName, String beverageDescription, boolean alcoholic, Long bottleSize, String name) {
		this.alcoholic = alcoholic;
		this.userName = userName;
		this.beverageDescription = beverageDescription;
		this.bottleSize = bottleSize;
		this.beverageSessionDto = new BeverageSessionDto(name, alcoholic);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getBeverageDescription() {
		return beverageDescription;
	}

	public void setBeverageDescription(String beverageDescription) {
		this.beverageDescription = beverageDescription;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public Long getBottleSize() {
		return bottleSize;
	}

	public void setBottleSize(Long bottleSize) {
		this.bottleSize = bottleSize;
	}

	public BeverageSessionDto getBeverageSessionDto() {
		return beverageSessionDto;
	}

	public void setBeverageSessionDto(BeverageSessionDto beverageSessionDto) {
		this.beverageSessionDto = beverageSessionDto;
	}
}
