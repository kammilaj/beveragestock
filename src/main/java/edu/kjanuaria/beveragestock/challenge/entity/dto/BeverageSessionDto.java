package edu.kjanuaria.beveragestock.challenge.entity.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class BeverageSessionDto {

	@NotNull
	@Size(min = 1, message = "code can't be null")
	private String code;
	@NotNull
	@Size(min = 1, message = "type alcoholic can't be null")
	private boolean alcoholic;
	private Long quantity;
	private Integer capacity;

	public BeverageSessionDto() {
		// TODO Auto-generated constructor stub
	}

	public BeverageSessionDto(String code, boolean alcoholic, Long quantity, Integer capacity) {
		this.code = code;
		this.alcoholic = alcoholic;
		this.capacity = capacity;
		this.quantity = quantity;
	}

	public BeverageSessionDto(String name, boolean alcoholic) {
		this.code = name;
		this.alcoholic = alcoholic;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public Integer getCapacity() {
		return capacity;
	}

	public void setCapacity(Integer capacity) {
		this.capacity = capacity;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
}
