package edu.kjanuaria.beveragestock.challenge.entity.dto;

import java.sql.Timestamp;

public class HistoryStockBeverageDto {
	// horário, tipo, volume, seção e responsável pela entrada.
	private Timestamp hour;
	private boolean alcoholic;
	private Long volume;
	private String userName;

	public HistoryStockBeverageDto() {
		// TODO Auto-generated constructor stub
	}

	public HistoryStockBeverageDto(Timestamp hour, boolean alcoholic, Long volume, String userName) {
		super();
		this.hour = hour;
		this.alcoholic = alcoholic;
		this.volume = volume;
		this.userName = userName;
	}

	public Timestamp getHour() {
		return hour;
	}

	public void setHour(Timestamp hour) {
		this.hour = hour;
	}

	public boolean isAlcoholic() {
		return alcoholic;
	}

	public void setAlcoholic(boolean alcoholic) {
		this.alcoholic = alcoholic;
	}

	public Long getVolume() {
		return volume;
	}

	public void setVolume(Long volume) {
		this.volume = volume;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}
