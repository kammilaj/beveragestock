package edu.kjanuaria.beveragestock.challenge.entity.dto;

public class SellBeverageDto {
	private Integer idBeverage;
	private String userName;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Integer getIdBeverage() {
		return idBeverage;
	}

	public void setIdBeverage(Integer idBeverage) {
		this.idBeverage = idBeverage;
	}

}
