package edu.kjanuaria.beveragestock.challenge.enumerators;

public enum BeverageStateEnum {

	INPUT("Input"), OUTPUT("Output");

	private String desc;

	private BeverageStateEnum(String desc) {
		this.desc = desc;
	}

	public String desc() {
		return desc;
	}
}
