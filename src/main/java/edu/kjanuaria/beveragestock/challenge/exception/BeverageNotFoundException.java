package edu.kjanuaria.beveragestock.challenge.exception;

public class BeverageNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BeverageNotFoundException(String exception) {
		super(exception);
	}
}
