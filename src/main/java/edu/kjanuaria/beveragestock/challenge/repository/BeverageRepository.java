package edu.kjanuaria.beveragestock.challenge.repository;

import edu.kjanuaria.beveragestock.challenge.entity.Beverage;
import edu.kjanuaria.beveragestock.challenge.entity.dto.BeverageDto;

public interface BeverageRepository {
	Beverage storeBeverage(BeverageDto beverage);

	public void sellBeverage(Integer id, String userName);
}
