package edu.kjanuaria.beveragestock.challenge.repository;

import java.util.List;

import edu.kjanuaria.beveragestock.challenge.entity.BeverageSession;

public interface IBeverageSessionRepository {
	List<BeverageSession> findSessionsAvailable(Long volume, boolean alcoholic);
}
