package edu.kjanuaria.beveragestock.challenge.service;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import edu.kjanuaria.beveragestock.challenge.entity.Beverage;
import edu.kjanuaria.beveragestock.challenge.repository.BeverageRepository;

public interface IBeverage extends CrudRepository<Beverage, Integer>, BeverageRepository {
	public List<Beverage> findAllByType(String id);

	@Query("SELECT SUM(b.bottleSize) FROM Beverage b WHERE b.alcoholic=?1")
	Long countBeverageByType(Boolean type);

}
