package edu.kjanuaria.beveragestock.challenge.service;

import java.sql.Timestamp;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import edu.kjanuaria.beveragestock.challenge.Application;
import edu.kjanuaria.beveragestock.challenge.entity.Beverage;
import edu.kjanuaria.beveragestock.challenge.entity.BeverageSession;
import edu.kjanuaria.beveragestock.challenge.entity.HistoryStockBeverage;
import edu.kjanuaria.beveragestock.challenge.entity.dto.BeverageDto;
import edu.kjanuaria.beveragestock.challenge.enumerators.BeverageStateEnum;
import edu.kjanuaria.beveragestock.challenge.exception.BeverageNotFoundException;
import edu.kjanuaria.beveragestock.challenge.exception.SessionDontSupportVolumeException;
import edu.kjanuaria.beveragestock.challenge.exception.SessionNotAvailableException;
import edu.kjanuaria.beveragestock.challenge.exception.SessionNotFoundException;
import edu.kjanuaria.beveragestock.challenge.repository.BeverageRepository;
import edu.kjanuaria.beveragestock.challenge.util.BeverageUtils;
import edu.kjanuaria.beveragestock.challenge.util.ConverterUtils;

public class IBeverageImpl implements BeverageRepository {

	private static final Logger log = LoggerFactory.getLogger(Application.class);

	@Autowired
	private IBeverageSession iBeverageSession;

	@Autowired
	private IHistoryBeverage iHistoryBeverage;

	@Autowired
	private IBeverage iBeverage;

	@Override
	public Beverage storeBeverage(BeverageDto beverage) {

		BeverageSession selectedSession = iBeverageSession
				.getBeverageSessionByName(beverage.getBeverageSessionDto().getCode());

		if (Objects.isNull(selectedSession)) {
			throw new SessionNotFoundException("Não existe a sessão " + beverage.getBeverageSessionDto().getCode());
		} else {
			// verifica se sao dos mesmo tipo senao checar historico
			if (selectedSession.isAlcoholic() != beverage.isAlcoholic()) {
				selectedSession = iBeverageSession.isSessionAvailable(beverage.getBeverageSessionDto().getCode());
				if (Objects.isNull(selectedSession)) {
					throw new SessionNotAvailableException("Sessão não pode receber esse tipo de bebida no momento");
				} else {
					selectedSession.setAlcoholic(beverage.isAlcoholic());
					selectedSession.setCapacity(BeverageUtils.getCapacity(beverage.isAlcoholic()));
				}
			}
			// verifica se a sessao suporta o volume de bebidas
			if (Boolean.FALSE.equals(BeverageUtils.canBeAllocated(selectedSession.getCapacity(),
					beverage.getBottleSize(), selectedSession.getQuantity()))) {
				throw new SessionDontSupportVolumeException("Sessão não suporta o volume " + beverage.getBottleSize());
			} else {
				selectedSession.setAlcoholic(beverage.isAlcoholic());
				selectedSession.setQuantity(beverage.getBottleSize());
			}
		}

		BeverageSession save = iBeverageSession.save(selectedSession);
		Beverage entity = ConverterUtils.convertDtoToEntity(beverage);

		// setando sessao
		entity.setBeverageSession(selectedSession);

		// salvar historico
		HistoryStockBeverage history = iHistoryBeverage
				.save(createHistoryEntity(beverage, save, BeverageStateEnum.INPUT));
		log.info("Created history: " + history.getUserName() + " - " + history.getBeverageSession() + " - "
				+ history.getHour());
		return iBeverage.save(entity);

	}

	/**
	 * Registra saída de bebida
	 */
	@Override
	public void sellBeverage(Integer id, String userName) {

		// retornar informações para salvar historico
		Optional<Beverage> findById = iBeverage.findById(id);

		if (!findById.isPresent()) {
			throw new BeverageNotFoundException("Bebida não existe");
		}

		Beverage beverage = findById.get();

		HistoryStockBeverage sellHistoryEntity = sellHistoryEntity(beverage.getBottleSize(), beverage.isAlcoholic(),
				beverage.getBeverageSession(), userName);

		// retira do estoque
		iBeverage.deleteById(id);

		// salva historico
		HistoryStockBeverage history = iHistoryBeverage.save(sellHistoryEntity);

		// loga informação no console
		log.info("Sell history: " + history.getUserName() + " - " + history.getBeverageSession() + " - "
				+ history.getHour());
	}

	/**
	 * Cria o registro de entrada
	 * 
	 * @param beverage
	 * @param idBeverageSession
	 * @return
	 */
	private HistoryStockBeverage createHistoryEntity(BeverageDto beverage, BeverageSession save,
			BeverageStateEnum beverageStateEnum) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		HistoryStockBeverage historyStockBeverage = new HistoryStockBeverage(beverage.getBottleSize(),
				beverage.isAlcoholic(), save, beverageStateEnum, timestamp, beverage.getUserName());
		return historyStockBeverage;
	}

	/**
	 * Criar registro de saída
	 * 
	 * @param volume
	 * @param alcoholic
	 * @param beverageSession
	 * @param userName
	 * @return
	 */
	private HistoryStockBeverage sellHistoryEntity(Long volume, boolean alcoholic, BeverageSession beverageSession,
			String userName) {
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		HistoryStockBeverage historyStockBeverage = new HistoryStockBeverage(volume, alcoholic, beverageSession,
				BeverageStateEnum.OUTPUT, timestamp, userName);
		return historyStockBeverage;
	}

}
