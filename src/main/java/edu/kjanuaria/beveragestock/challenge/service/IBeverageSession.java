package edu.kjanuaria.beveragestock.challenge.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import edu.kjanuaria.beveragestock.challenge.entity.BeverageSession;
import edu.kjanuaria.beveragestock.challenge.repository.IBeverageSessionRepository;

public interface IBeverageSession extends CrudRepository<BeverageSession, Integer>, IBeverageSessionRepository {

	@Query(value = "SELECT b.* FROM BEVERAGE_SESSION b WHERE b.BOL_ALCOHOLIC = :isAlcoholic", nativeQuery = true)
	List<BeverageSession> getAvailableSession(@Param("isAlcoholic") Boolean isAlcoholic);

	@Query("SELECT b FROM BeverageSession b WHERE b.name = :name")
	BeverageSession getBeverageSessionByName(@Param("name") String name);

	@Query(value = "SELECT b.* FROM beverage_session b LEFT OUTER JOIN history_stock_beverage hb ON hb.beverage_session_id = b.id WHERE b.bol_alcoholic = :isAlcoholic and b.quantity IS NULL or  b.quantity = 0 AND   hb.hour < SYSDATE - 1 order by  hb.hour  asc limit 1", nativeQuery = true)
	BeverageSession getAvailableSessionLastDay(@Param("isAlcoholic") Boolean isAlcoholic);

	@Query(value = "SELECT b.* FROM beverage_session b LEFT OUTER JOIN history_stock_beverage hb ON hb.beverage_session_id = b.id WHERE b.name= :name AND (hb.beverage_session_id is null  OR (hb.beverage_session_id is not null and  hb.hour < SYSDATE - 1 )) ", nativeQuery = true)
	BeverageSession isSessionAvailable(@Param("name") String name);

	@Query(value = "SELECT b.*,hb.* FROM beverage_session b LEFT OUTER JOIN history_stock_beverage hb ON hb.beverage_session_id = b.id WHERE (hb.beverage_session_id is null  OR (hb.beverage_session_id is not null and  hb.hour > :data)) ", nativeQuery = true)
	List<BeverageSession> isSessionAvailable(@Param("data") Date data);

	@Query(value = "SELECT b FROM BeverageSession b LEFT JOIN fetch b. hb ON hb.beverage_session_id = b.id WHERE (hb.beverage_session_id is null  OR (hb.beverage_session_id is not null and  hb.hour > :data)) ", nativeQuery = true)
	List<BeverageSession> sessionsAndLas24HoursHistory(@Param("data") Date data);

}
