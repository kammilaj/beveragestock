package edu.kjanuaria.beveragestock.challenge.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import edu.kjanuaria.beveragestock.challenge.entity.BeverageSession;
import edu.kjanuaria.beveragestock.challenge.entity.HistoryStockBeverage;
import edu.kjanuaria.beveragestock.challenge.repository.IBeverageSessionRepository;
import edu.kjanuaria.beveragestock.challenge.util.BeverageUtils;

public class IBeverageSessionImpl implements IBeverageSessionRepository {

	@Autowired
	private IBeverageSession iBeverageSession;

	/**
	 * Consulta dos locais disponíveis de armazenamento de um determinado volume
	 * de bebida
	 * 
	 */
	@Override
	public List<BeverageSession> findSessionsAvailable(Long volume, boolean alcoholic) {

		List<BeverageSession> sessionAvailable = iBeverageSession
				.isSessionAvailable(BeverageUtils.getDateSubtracted24Hours());

		List<BeverageSession> avaliableSessions = new ArrayList<BeverageSession>();

		Map<BeverageSession, List<HistoryStockBeverage>> mapBeverageHistory = new HashMap<>();
		sessionAvailable.forEach(p -> mapBeverageHistory.put(p, p.getHistoryStockBeverages()));

		// verifica se o volume cabe calculando o volume com a capacidade
		// verifica se a sessão pode receber esse tipo de bebida
		Map<BeverageSession, List<HistoryStockBeverage>> collect = mapBeverageHistory.entrySet().stream()
				.filter(session -> Boolean.TRUE.equals(BeverageUtils.canBeAllocated(session.getKey().getCapacity(),
						volume, session.getKey().getQuantity())))
				.filter(session -> session.getValue().stream()
						.allMatch(history -> Objects.equals(history.getAlcoholic(), Boolean.valueOf(alcoholic))))
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		collect.keySet().iterator().forEachRemaining(avaliableSessions::add);

		return avaliableSessions;
	}

}
