package edu.kjanuaria.beveragestock.challenge.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import edu.kjanuaria.beveragestock.challenge.entity.HistoryStockBeverage;

public interface IHistoryBeverage extends CrudRepository<HistoryStockBeverage, Long> {

	@Query("select hsb from HistoryStockBeverage hsb inner join hsb.beverageSession b where hsb.alcoholic = :isAlcoholic order by hsb.hour asc, b.name asc")
	List<HistoryStockBeverage> findAllByIsAlcoholic(@Param("isAlcoholic") Boolean isAlcoholic);

	@Query("select hsb from HistoryStockBeverage hsb inner join hsb.beverageSession b where b.name = :session order by hsb.hour asc, b.name asc")
	List<HistoryStockBeverage> findAllBySession(@Param("session") String session);

	@Query("select hsb from HistoryStockBeverage hsb inner join hsb.beverageSession b where hsb.hour > :data")
	List<HistoryStockBeverage> findLast24HoursBySessionId(@Param("data") Date data);

}
