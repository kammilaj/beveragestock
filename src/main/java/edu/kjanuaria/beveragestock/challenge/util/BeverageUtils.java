package edu.kjanuaria.beveragestock.challenge.util;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Objects;

import net.logstash.logback.encoder.org.apache.commons.lang.BooleanUtils;

public class BeverageUtils {
	/**
	 * Define capacity
	 * 
	 * @param isAlcoholic
	 * @return
	 */
	public static int getCapacity(Boolean isAlcoholic) {
		if (BooleanUtils.isFalse(isAlcoholic)) {
			return 400;
		}
		return 500;
	}

	/**
	 * Verifica se a bebida não excede o limite da capacidade da sessão
	 * 
	 * @param capacity
	 * @param mililiters
	 * @param quantity
	 * @return
	 */
	public static Boolean canBeAllocated(Integer capacity, Long mililiters, Long quantity) {
		// verificacao só deve ser feita se a quantidade ja existente de bebidas
		// na sessao for diferente de nula
		if (!Objects.isNull(quantity)) {
			BigDecimal capacityInMiliLiters = ConverterUtils.convertToMiliLiters(capacity);
			int compareTo = new BigDecimal(mililiters + quantity).compareTo(capacityInMiliLiters);
			// se o futuro for maior retorna false
			if (Objects.equals(1, compareTo)) {
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}

	/**
	 * subtract days to date in java
	 * 
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date getDateSubtracted24Hours() {
		Date date = java.util.Calendar.getInstance().getTime();
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.DATE, -1);

		return cal.getTime();
	}

}
