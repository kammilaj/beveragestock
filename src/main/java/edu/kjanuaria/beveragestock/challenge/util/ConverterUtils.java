package edu.kjanuaria.beveragestock.challenge.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import edu.kjanuaria.beveragestock.challenge.entity.Beverage;
import edu.kjanuaria.beveragestock.challenge.entity.BeverageSession;
import edu.kjanuaria.beveragestock.challenge.entity.HistoryStockBeverage;
import edu.kjanuaria.beveragestock.challenge.entity.dto.BeverageDto;
import edu.kjanuaria.beveragestock.challenge.entity.dto.BeverageSessionDto;
import edu.kjanuaria.beveragestock.challenge.entity.dto.HistoryStockBeverageDto;

public class ConverterUtils {

	/**
	 * @param mililiters
	 * @return
	 */
	public static BigDecimal convertToLiters(Long value) {

		BigDecimal mililiters = BigDecimal.valueOf(value);
		BigDecimal oneLiter = BigDecimal.valueOf(1000L);

		BigDecimal calculatedLiters = mililiters.divide(oneLiter, 3, BigDecimal.ROUND_CEILING);

		return calculatedLiters;
	}

	public static BigDecimal convertToMiliLiters(Integer value) {

		BigDecimal mililiters = BigDecimal.valueOf(value);
		BigDecimal oneLiter = BigDecimal.valueOf(1000L);

		BigDecimal calculatedLiters = mililiters.multiply(oneLiter);

		return calculatedLiters;
	}

	public static Beverage convertDtoToEntity(BeverageDto beverageDto) {
		Beverage entity = new Beverage(beverageDto.getBeverageDescription(), beverageDto.isAlcoholic(),
				beverageDto.getBottleSize());

		return entity;
	}

	public static BeverageDto convertEntityToDto(Beverage entity) {
		BeverageDto beverageDto = new BeverageDto(entity.getCode(), entity.isAlcoholic(), entity.getBottleSize(),
				entity.getBeverageSession().getQuantity(), entity.getBeverageSession().getCapacity(),
				entity.getBeverageSession().getName());

		return beverageDto;
	}

	public static BeverageSession convertDtoToEntity(BeverageSessionDto beverageSessionDto) {
		BeverageSession entity = new BeverageSession(beverageSessionDto.isAlcoholic(), beverageSessionDto.getQuantity(),
				beverageSessionDto.getCapacity(), beverageSessionDto.getCode());

		return entity;
	}

	public static BeverageSessionDto convertEntityToDto(BeverageSession entity) {
		BeverageSessionDto beverageDto = new BeverageSessionDto(entity.getName(), entity.isAlcoholic(),
				entity.getQuantity(), entity.getCapacity());

		return beverageDto;
	}

	public static HistoryStockBeverageDto convertEntityToDto(HistoryStockBeverage entity) {
		HistoryStockBeverageDto historyStockBeverageDto = new HistoryStockBeverageDto(entity.getHour(),
				entity.getAlcoholic(), entity.getVolume(), entity.getUserName());

		return historyStockBeverageDto;
	}

	public static List<HistoryStockBeverageDto> convertEntityToDto(List<HistoryStockBeverage> listEntity) {

		List<HistoryStockBeverageDto> history = new ArrayList<>();
		listEntity.forEach(entity -> history.add(ConverterUtils.convertEntityToDto(entity)));

		return history;
	}

	public static List<BeverageSessionDto> convertListEntityToDto(List<BeverageSession> listEntity) {

		List<BeverageSessionDto> history = new ArrayList<>();
		listEntity.forEach(entity -> history.add(ConverterUtils.convertEntityToDto(entity)));

		return history;
	}

}
